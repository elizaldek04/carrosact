// Función para calcular el pago mensual
function calcularPagoMensual(precio, plazo) {
    let tasaInteresAnual = 0.1; // Tasa de interés anual del 10% (puedes ajustarla según tus necesidades)
    let tasaInteresMensual = tasaInteresAnual / 12;
    let cuotas = plazo;
    let pagoMensual = (precio * tasaInteresMensual) / (1 - Math.pow(1 + tasaInteresMensual, -cuotas));
    return pagoMensual.toFixed(2);
}

// Función para calcular el total a financiar
function calcularTotalFinanciar(precio, porcentaje) {
    let montoFinanciar = precio * (1 - porcentaje / 100);
    return montoFinanciar.toFixed(2);
}

// Función para limpiar los campos de entrada
function limpiarCampos() {
    document.getElementById('descripcion').value = '';
    document.getElementById('idPrecio').value = '';
    document.getElementById('idPorcentaje').value = '';
    document.getElementById('idPlazo').value = '12';
    document.getElementById('idPagoInicial').value = '';
    document.getElementById('idPagoMensual').value = '';
}

// Asignar evento de clic al botón de calcular
const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function () {
    let precio = parseFloat(document.getElementById('idPrecio').value);
    let porcentaje = parseFloat(document.getElementById('idPorcentaje').value);
    let plazo = parseInt(document.getElementById('idPlazo').value);

    // Calcular el pago mensual y el total a financiar
    let pagoMensual = calcularPagoMensual(precio, plazo);
    let totalFinanciar = calcularTotalFinanciar(precio, porcentaje);

    // Mostrar los resultados en los campos de texto
    document.getElementById('idPagoMensual').value = pagoMensual;
    document.getElementById('idPagoInicial').value = totalFinanciar;
});

// Asignar evento de clic al botón de limpiar
const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', limpiarCampos);